from flask import Flask, request
import wakeonlan
import json
import os


app = Flask(__name__)


@app.route('/')
def send_wakeonlan_package():
    with open('machines_info.json') as json_data:
        machines_info = json.load(json_data)
    machine_name = request.args.get('machine')
    machine_info = machines_info.get(machine_name, None)
    if machine_info is None:
        return 'There is no machine registered with name ' + machine_name

    if 'local_ip' in machine_info and os.system('ping -c 1 ' + machine_info['local_ip']) == 0:
        return 'Target machine ' + machine_name + ' is already on'
    else:
        wakeonlan.send_magic_packet(machine_info['mac'])
        return 'Sending WakeOnLan package for machine ' + machine_name


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
